#------------------------------------------------------------------------------
# Terraform module for create GKE clusters with managed nodepools
#------------------------------------------------------------------------------

resource "google_container_cluster" "gke" {
  provider           = "google-beta"
  name               = "my-infra-gke"
  network            = var.network_name
  subnetwork         = var.gke_subnet_name
  min_master_version = var.master_version
  location           = var.region
  node_locations     = var.node_locations
  project            = var.project_id

  # Setting an empty username and password explicitly disables basic auth
  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = false
    }

  }

  # node pool and immediately delete it. To manage it seperately
  remove_default_node_pool = true
  initial_node_count       = 1

  # From which networks that master should accessible
  master_authorized_networks_config {
    cidr_blocks {
        cidr_block = "0.0.0.0/0"
        display_name = "public-access"
    }
  }




  # Subranges will be created automatically /14 for pods | /19 for services
  # If not you can pass these values as variables.
  ip_allocation_policy {
    cluster_secondary_range_name  = var.ip_range_pods
    services_secondary_range_name = var.ip_range_services
  }

  ## ISTIO addon, make it false to isntall
  addons_config {
    istio_config {
      disabled = true
    }
  }


  maintenance_policy {
    daily_maintenance_window {
      start_time = var.maintenance_window
    }
  }
}

resource "google_container_node_pool" "core_node_pool" {
  name       = "core-node-pool"
  location   = var.region
  cluster    = google_container_cluster.gke.name
  node_count = var.core_node_pool_count
  project    = var.project_id
  version    = var.node_version
  

  node_config {
    machine_type = var.node_machine_type
    preemptible  = true
    disk_size_gb = var.disk_size
    disk_type = var.disk_type

    oauth_scopes = [
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/cloud_debugger",
      "https://www.googleapis.com/auth/cloud-platform",
      "https://www.googleapis.com/auth/service.management.readonly",
      "https://www.googleapis.com/auth/servicecontrol",
      "https://www.googleapis.com/auth/trace.append",
    ]

    labels = var.node_labels
    tags   = var.node_tags
  }

  management {
    auto_upgrade = var.nodes_auto_upgrade
    auto_repair  = var.nodes_auto_repair
  }
}