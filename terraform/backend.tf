terraform {
  backend "gcs" {
    bucket = "<your-gcs-bucket-name-id>"
    prefix = "terraform-state"
    # Project is not needed since backend is already created
    project = "<your-project-id>"
    credentials = "/patth/to/gcs-bucket@<your-project-id>.iam.gserviceaccount.com"
  }
}
