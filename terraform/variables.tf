variable "project_id" {
  default = "your-gcp-project"
}


variable "region" {
  default = "us-central1"
}


variable "zone" {
  default = "us-central1-a"
}

variable "network_name" {
  default = "infra-vpc"
}

variable "gke_subnet_name" {
  default = "gke-nodes"
}

variable "node_locations" {
  default = ["us-central1-a"]
}

variable "ip_range_pods" {
  default = "gke-pods"
}

variable "ip_range_services" {
  default = "gke-services"
}


variable "compute_engine_service_account" {
  default = "project-service-account@<your-project-id>.iam.gserviceaccount.com"
}

variable "master_version" {
  default = "1.13.9-gke.3"
}

variable "node_version" {
  default = "1.13.9-gke.3"

}

variable "maintenance_window" {
  default = "00:00"

}



variable "core_node_pool_count" {
  default = 1
}

variable "node_machine_type" {
  default = "g1-small"
}

variable "nodes_auto_upgrade" {
  description = "Auto upgrade nodes"
  default     = false
}

variable "nodes_auto_repair" {
  description = "Auto repair nodes"
  default     = true
}

variable "node_labels" {
  description = "Node labels"
  type        = "map"
  default = {
    env = "prod",
    app = "web",
  }
}

variable "node_tags" {
  description = "Node tags"
  type        = "list"
  default     = ["prod", "web"]
}

variable "disk_size" {
    default = 15
}

variable disk_type {
    default = "pd-standard"
}


