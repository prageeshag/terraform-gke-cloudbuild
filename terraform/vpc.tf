module "vpc" {
  source  = "terraform-google-modules/network/google"
  version = "~> 1.0.0"

  project_id   = var.project_id
  network_name = var.network_name
  routing_mode = "GLOBAL"

  subnets = [
    {
      subnet_name           = "gke-nodes"
      subnet_ip             = "10.8.100.0/23"
      subnet_region         = var.region
      subnet_private_access = "true"
    }
  ]



  secondary_ranges = {
    "gke-nodes" = [
      {
        range_name    = "gke-pods"
        ip_cidr_range = "10.220.0.0/15"
      },
      {
        range_name    = "gke-services"
        ip_cidr_range = "10.80.100.0/24"
      },
    ]
  }

}