
provider "google" {
  credentials = "/home/prageesha/workspace/prageesha-gke-terraform/service-accounts/sa-terraform.json"
  project     = var.project_id
  region      = var.region
}

provider "google-beta" {
  credentials = "/home/prageesha/workspace/prageesha-gke-terraform/service-accounts/sa-terraform.json"
  project     = var.project_id
  region      = var.region
}